package cod.exam.ex1;

public class I {
    private long t;
    private K[] ks; //unidirectional association - I has one or more K-s (it can have no K-s at all)

    public void f() {

    }

    public void i(J j) {
        System.out.printf(j.toString()); //dependency - reference for class J in an operation (method)
    }
}

class J {

}

class N {
    I[] is; //aggregation
}

class K extends S { //generalization (mostenire) (class K is a class S)
    L l;

    K() {
        this.l = new L(); //composition - class K also constructs L
    }
}

class S {
    public void metB() {

    }
}

class L {
    public void metA() {

    }
}