package cod.exam.ex2;

import javax.swing.*;
import java.awt.*;
import java.io.*;

public class GraphicInterface extends JFrame {   //the graphical interface is a JFrame

    public GraphicInterface() {
        this.setLayout(null);
        this.setSize(200, 180);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        TextField tf1 = new TextField();
        tf1.setBounds(20, 20, 150, 20);

        TextField tf2 = new TextField();
        tf2.setBounds(20, 60, 150, 20);

        Button b1 = new Button("Count characters");
        b1.setBounds(40, 100, 100, 25);
        b1.addActionListener(e -> {
            String fileName = tf1.getText();
            if (validateFileName(fileName)) {
                tf2.setText(Integer.toString(countCharacters(fileName)));
            } else {        //i tried it out with the file "files/test1.txt" and it worked
                tf2.setText(String.format("%s is not a valid filename.", fileName));
            }
        });

        this.add(tf1);
        this.add(tf2);
        this.add(b1);
        this.setVisible(true);
    }

    private boolean validateFileName(String fileName) { //this method checks if filename valid and if the file is a valid file
        File file = new File(fileName);
        return file.exists() && file.isFile() && fileName.endsWith(".txt");
    }

    private int countCharacters(String fileName) {
        File file = new File(fileName);
        int characterCount = 0;

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String line;

            while ((line = bufferedReader.readLine()) != null) {
                characterCount += line.length(); //count the number of characters (new line char not counted)
            }

            bufferedReader.close();     //closing all readers
            inputStreamReader.close();
            fileInputStream.close();

        } catch (IOException exception) {
            exception.printStackTrace(); //handling exceptions
        }

        return characterCount;
    }
}

class Main {
    public static void main(String[] args) {
        new GraphicInterface();
    }
}
